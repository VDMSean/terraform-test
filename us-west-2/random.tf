resource "random_pet" "pet" {
  count = 3
}

output "pet" {
  value = random_pet.pet.*.id
}
