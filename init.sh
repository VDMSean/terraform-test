#!/bin/bash

set -e

# https://docs.gitlab.com/ce/user/infrastructure/

function die() {
  >&2 echo -e "${@}"
  exit 1
}

function gitlab_host() {
  GITLAB_HOST=""

  # try to guess gitlab hostname from git origin
  GIT_ORIGIN=$(git remote get-url --push origin 2>> /dev/null || echo -n "")
  if [[ $GIT_ORIGIN =~ ^git@ ]]; then
    GIT_ORIGIN_HOST=$(echo -n "${GIT_ORIGIN}" | sed -E 's/^git@([^:]+):.*$/\1/')
  elif [[ $GIT_ORIGIN =~ ^http ]]; then
    GIT_ORIGIN_HOST=$(echo -n "${GIT_ORIGIN}" | sed -E 's/^https?:\/\/([^/]+)\/.*$/\1/')
  fi

  read -r -p "Enter GitLab hostname [${GIT_ORIGIN_HOST}]: " GITLAB_HOST
  GITLAB_HOST=${GITLAB_HOST:-$GIT_ORIGIN_HOST}
  if [[ ! "${GITLAB_HOST}" =~ ^[A-Za-z0-9.-]+\.([A-Za-z0-9.-]+)+$ ]]; then
    die "Invalid hostname: \"${GITLAB_HOST}\""
  fi
}

function gitlab_project() {
  GITLAB_ORIGIN_PROJECT=$(echo -n "${GIT_ORIGIN}" | sed -E 's/\.git$//;s/^https?:\/\/[^\/]+\/(.*)$/\1/;s/^git@[^:]+:(.*)$/\1/')
  read -r -p "Enter GitLab project [${GITLAB_ORIGIN_PROJECT}]: " GITLAB_PROJECT
  GITLAB_PROJECT=${GITLAB_PROJECT:-$GITLAB_ORIGIN_PROJECT}
  if [ -z "${GITLAB_PROJECT}" ]; then
    die "GitLab project name can not be empty."
  fi
  GITLAB_PROJECT_URLENCODE=$(echo -n "${GITLAB_PROJECT}" | jq -sRr @uri)
}

function gitlab_user() {
  if [[ $GIT_ORIGIN =~ ^git@ ]]; then
    GIT_SSH_ADDR=$(echo -n "${GIT_ORIGIN}" | sed -E 's/:.*$//')
    GIT_SSH_BANNER=$(ssh -o ConnectTimeout=3 "${GIT_SSH_ADDR}" 2>> /dev/null || echo -n "")
    GIT_SSH_USER=$(echo -n "${GIT_SSH_BANNER}" | sed -E 's/^Welcome to GitLab, @([^\!]+)\!$/\1/')
  fi

  echo -e "Your GitLab username can be found here: https://${GITLAB_HOST}/profile/account"
  read -r -p "Enter GitLab username [${GIT_SSH_USER}]: " GITLAB_USER
  GITLAB_USER=${GITLAB_USER:-$GIT_SSH_USER}

  if [ -z "${GITLAB_USER}" ]; then
    die "GitLab username can not be empty."
  fi
}

function gitlab_token() {
  echo -e "Your GitLab tokens can be found here: https://${GITLAB_HOST}/profile/personal_access_tokens"
  read -r -p "Enter GitLab token with 'api' scope: " GITLAB_TOKEN
  if [ -z "${GITLAB_TOKEN}" ]; then
    die "GitLab token can not be empty."
  fi
}

function gitlab_project_id() {
  GITLAB_PROJECT_JSON=$(curl -s --connect-timeout 5 -H "Authorization: Bearer ${GITLAB_TOKEN}" --url "https://${GITLAB_HOST}/api/v4/projects/${GITLAB_PROJECT_URLENCODE}" || echo -n "")
  GITLAB_PROJECT_JSON_ID=$(echo -n "${GITLAB_PROJECT_JSON}" | jq -r .id 2>> /dev/null || echo -n "")

  echo -e "GitLab Project ID can be found at https://${GITLAB_HOST}/${GITLAB_PROJECT}"
  read -r -p "GitLab Project ID [${GITLAB_PROJECT_JSON_ID}]: " GITLAB_PROJECT_ID
  GITLAB_PROJECT_ID=${GITLAB_PROJECT_ID:-$GITLAB_PROJECT_JSON_ID}
  if [[ ! "${GITLAB_PROJECT_ID}" =~ ^[0-9]+$ ]]; then
    die "Project ID [${GITLAB_PROJECT_ID}] should be an positive integer."
  fi
}

function terraform_init() {
  ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  cd "${ROOT}" || die could not change to source directory

  echo "Initializing terraform in directories at project root that contain backend.tf ..."

  for DIR in *; do
    if [ -d "${DIR}" ]; then
      cd "${ROOT}/${DIR}" || die "could not cd to ${DIR}"
        if [ -f "backend.tf" ]; then
          echo "Initializing terraform in ${DIR} ..."
          TARGET_STATE_NAME="${DIR}"
          TF_ADDRESS="https://${GITLAB_HOST}/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${TARGET_STATE_NAME}"
          terraform init \
            -backend-config="address=${TF_ADDRESS}" \
            -backend-config="lock_address=${TF_ADDRESS}/lock" \
            -backend-config="unlock_address=${TF_ADDRESS}/lock" \
            -backend-config="username=${GITLAB_USER}" \
            -backend-config="password=${GITLAB_TOKEN}" \
            -backend-config="lock_method=POST" \
            -backend-config="unlock_method=DELETE" \
            -backend-config="retry_wait_min=5"
        fi
    fi
  done

  echo "Done!"
}

gitlab_host
gitlab_project
gitlab_user
gitlab_token
gitlab_project_id
terraform_init
